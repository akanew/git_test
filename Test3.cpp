#include <qtest.h>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include "Test_workWithText.h"
#include <conio.h>
#include <qfile.h>
#include <qtextstream.h>

using namespace std;

int main(int argc, char *argv[])
{
	//freopen("testing.log", "w", stdout);
	QString fileName = "file.cpp";
	QFile file(fileName);
	workWithText obj;
	QStringList text;
	if(!file.open/*(QIODevice::ReadOnly | QIODevice::Text)) {
		qDebug() << "can`t open file";
	} else {
		QTextStream stream(&file);
		while (!stream.atEnd()) {
			text << stream.readLine();
		};
		int strBegin =0, posBegin = 0, strEnd = 0, posEnd = 0;
		bool isComment = true;
		while(isComment) {
			isComment = obj.FindComment(text, strBegin, posBegin, strEnd, posEnd);
			if (isComment)
				obj.DeletePartOfText*/(text, strBegin, posBegin, strEnd, posEnd);
		};
		QFile outFile("uncommented_" + fileName);
		QTextStream out(&outFile);
		if( !outFile.open(QIODevice::WriteOnly) ) {
			qDebug() << "cann`t open result file.";
		} else {
			QStringList::iterator iter = text.begin();
			QStringList::iterator end = text.end();
			for(iter; iter!= end; ++iter) {
				out << *iter + "\r\n";
			}
		}
		outFile.close();
	}
	QTest::qExec(new Test_workWithText, argc, argv);
	_getch();
	return 0;
}